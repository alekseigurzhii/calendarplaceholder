import logging, os
from ics import Calendar, Event
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler

CE, TITLE, DESCRIPTION, STARTTIME, ENDTIME = range(5)

def create_ics(update, context):
    user_calendar.events.add(user_event)
    for line in user_calendar:
        line = str(line)
        if 'DTSTART' or 'DTEND' in line:
            line = line.replace('Z', '')
        with open('user_event.ics', 'a') as ics:
            ics.writelines(line)
    update.message.reply_document(document=open('user_event.ics', 'rb'), filename=f'{user_event.name}.ics')
    os.remove('user_event.ics')
    return True


def start(update, context):
    reply_keyboard = [['Yes', 'No']]

    update.message.reply_text(
        'Hi! Do you want to create new appointment?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CE


def create_event(update, context):
    update.message.reply_text(
        'Title: ')
    return TITLE


def title(update, context):
    user_event.name = update.message.text
    user = update.message.from_user
    update.message.reply_text('Okay, now let\'s specify description '
                              'or send /skip if you don\'t want to.',
                              reply_markup=ReplyKeyboardRemove())

    return DESCRIPTION


def description(update, context):
    user_event.description = update.message.text
    update.message.reply_text('Set up start date, format \"YYYY-MM-DD - HH:MM\", '
                              'example: 2020-08-16 - 21:00')

    return STARTTIME


def skip_description(update, context):
    update.message.reply_text('Set up start date, format \"YYYY-MM-DD - HH:MM\", '
                              'example: 2020-08-16 - 21:00')

    return STARTTIME


def startTime(update, context):
    user_event.begin = update.message.text
    update.message.reply_text('Specify end date, format \"YYYY-MM-DD - HH:MM\", '
                              'example: 2020-08-16 - 21:00 '
                              'or send /skip if you don\'t want to.')

    return ENDTIME


def endTime(update, context):
    user_event.end = update.message.text
    update.message.reply_text('This is your event, just import it to your calendar')
    if create_ics(update, context):
        return ConversationHandler.END


def skip_endTime(update, context):
    update.message.reply_text('This is your event, just import it to your calendar ')
    if create_ics(update, context):
        return ConversationHandler.END


def cancel(update, context):
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def main():
    updater = Updater("1269406300:AAEG4hq622twPazSaYPFofpCaZ7thxbfde0", use_context=True)

    dp = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            CE: [MessageHandler(Filters.regex('^Yes$'), create_event)],

            TITLE: [MessageHandler(Filters.text, title)],

            DESCRIPTION: [MessageHandler(Filters.text & ~Filters.command, description),
                          CommandHandler('skip', skip_description)],

            STARTTIME: [MessageHandler(Filters.text, startTime)],

            ENDTIME: [MessageHandler(Filters.text & ~Filters.command, endTime),
                      CommandHandler('skip', skip_endTime)]
        },

        fallbacks=[CommandHandler('cancel', cancel), CommandHandler('No', cancel)]
    )

    dp.add_handler(conv_handler)

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    user_calendar = Calendar()
    user_event = Event()
    main()
