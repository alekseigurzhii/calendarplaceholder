FROM python:3
COPY ./ /app
WORKDIR /app
RUN pip install --upgrade ipython && pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["calendarPlaceHolderBot.py"]